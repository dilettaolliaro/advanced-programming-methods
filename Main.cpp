// generate graph(s)
// apply planarity test
//if not planar make it so

#include <iostream>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/boyer_myrvold_planar_test.hpp>
#include <tuple>
#include <vector>
#include "Support.hpp"

using namespace std;
 
int main(int argc, char** argv)
{
     using namespace boost;

    //construct an undirected weighted graph
    gr G(7);
    add_edge(0, 1, 2, G);
    add_edge(0, 2, 7, G);
    add_edge(0, 3, 8, G);
    add_edge(0, 4, 3, G);
    add_edge(1, 2, 5, G);
    add_edge(1, 3, 6, G);
    add_edge(1, 4, 9, G);
    add_edge(2, 3, 3, G);
    add_edge(2, 4, 1, G);
    add_edge(3, 4, 10, G);
    add_edge(5, 0, 4, G);
    add_edge(5, 1, 7, G);
    add_edge(5, 2, 3, G);
    add_edge(5, 3, 2, G);
    add_edge(6, 0, 10, G);
    add_edge(6, 1, 2, G);
    add_edge(6, 2, 4, G);
    add_edge(6, 4, 1, G);
    add_edge(6, 5, 5, G);
    add_edge(6, 3, 2, G);

    //We create the data structures needed to make our graph planar
    Support graph = Support(G);
    std::cout << "ORIGINAL GRAPH" << std::endl;
    graph.printBoostGraph();
    //The value of this variable will indicate if we intend to use the Kuratowski reduction or not
    //N.B: we do not obtain the optimal solution using it, but we just reduce the complexity
    //as far as constants are concerned.
    bool reduction;
    std::cout << "Type 1 if you want to use Kuratowsi Reduction, 0 otherwise" << std::endl;
    cin >> reduction;
    //We call the function that does the whole work, it also returns the sum 
    //of the weights of the erased edges
    int sum = graph.makePlanar(reduction);
    //We check another time (just to be sure) that the resulting graph is planar
    if (graph.isPlanar())  {
        std::cout << "G is planar and is now composed as follows: " << std::endl;
        graph.printBoostGraph();
        std::cout << "The total of the cut edges weight is: "<< sum << std::endl;
    }else{
        std::cout << "SOMETHING IS WRONG" << std::endl;
    }
    return 0;
}
