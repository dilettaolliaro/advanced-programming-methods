#include <iostream>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/boyer_myrvold_planar_test.hpp>
#include <tuple>
#include <vector>
#include <ostream>
#include <random>
#include <algorithm>



using namespace boost; 

typedef boost::property<boost::edge_weight_t, int> EdgeWeightProperty;
typedef adjacency_list<vecS, vecS, undirectedS, no_property, EdgeWeightProperty> gr;                       
typedef boost::graph_traits<gr>::edge_iterator edge_iterator;
typedef std::pair<edge_iterator, edge_iterator> EdgePair;
typedef std::vector<tuples::tuple< graph_traits<gr>::edge_descriptor,  int > > my_tuple;
typedef std::vector<gr::edge_descriptor> tobe_erased;
typedef std::vector<my_tuple> my_vector;
typedef boost::graph_traits<gr>::vertex_descriptor VertexDescriptor;
typedef std::vector< graph_traits<gr>::edge_descriptor > kuratowski_edges_t;
typedef boost::property<boost::edge_index_t, int, boost::property<boost::edge_weight_t, int> > KuratowskiEdgeProp;
typedef adjacency_list < vecS,vecS,undirectedS,property<vertex_index_t, int>, property<edge_index_t, int> > kuratowskiGraph;
class Support{
    int tocut;
    my_tuple graph;
    std::vector<my_tuple> combinations;
    gr &G;
     boost::property_map<gr, boost::edge_weight_t>::type EdgeWeightMap;
    public:
        //constructor
        Support(gr& g);
        //prints the graph represented by the vector of tuple
        void printGraph();
        //prints the Boost graph
        void printBoostGraph();
        //erases from the graph the edges contained in combo
        void eraseEdges(my_tuple combo);
        //adds in the graph the edges contained in combo
        void addEdges(my_tuple combo);
        //returns the number of edges we need to cut to make the graph planar
        int getTocut();
        //performs the planarity test
        bool isPlanar();
        //makes the graph planar
        int makePlanar(bool reduction);
        //deletes from the graph the edges that are not contained in a Kuratowski subgraph
        void getKuratowskiReduction();
        //generates all the possible combinations of "tocut" edges starting from the set of edges of the graph
        void genCombos(int k, int start, int currLen, bool* used, my_vector* res);
        //returns the best combination of edges to delete to have a planar graph but cutting away the 
        //minimal sum of edges weights.
        my_tuple getBestCombo(my_vector combos);
};


