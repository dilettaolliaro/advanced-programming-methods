\documentclass[12pt]{report}
\renewcommand{\baselinestretch}{1.5}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[dvipsnames]{xcolor}
\usepackage[T1]{fontenc}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{qtree}
\usepackage{listings}
\usepackage{tikz}
\usepackage{centernot}
\usepackage{comment}
\usepackage{hyperref}
\usepackage{mdframed}
\usepackage{verbatim}
\usepackage{fancyvrb}
\usepackage{tabularx}
\usepackage{graphicx}
\usepackage{multirow}
\usepackage{caption}
\usepackage{float}
\usepackage{pxfonts}
\usepackage{pifont}
\usepackage{enumitem}
\usepackage{adforn}
\usepackage{amsmath}
\usepackage{calligra}
\usepackage[T1]{fontenc}
\usepackage{bbding}
\usepackage{textcomp}
\usepackage{tikz}
\usepackage[export]{adjustbox}
\tikzstyle{mybox} = [draw=black, very thick, rectangle, rounded corners, inner ysep=5pt, inner xsep=5pt]
\setlength{\footnotesep}{1.\baselineskip}%



%\usepackage{ntheorem}
\usetikzlibrary{shapes}
\usetikzlibrary{arrows}
\usetikzlibrary{calc}
\usepackage[
type={CC},
modifier={by-nc-sa},
version={4.0},
lang={english},
]{doclicense}
\lstset{
	basicstyle=\ttfamily,
	mathescape,
	tabsize=4
}

\lstset{
	basicstyle=\ttfamily,
	columns=fullflexible,
	breaklines=true,
	escapeinside={@|}{|@}
}
\setcounter{secnumdepth}{4}
\setcounter{tocdepth}{4}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{example}{Example}
\newtheorem{lemma}{Lemma}
\newcommand{\splitcell}[2][c]{%
  \begin{tabular}[c]{@{}c@{}}\strut#2\strut\end{tabular}%
}
\begin{document}
\date{A.A. 2019/2020}
\author{Diletta Olliaro\\\ \\ Dipartimento di Scienze Ambientali, Informatica e Statistica\\ Università Ca'Foscari Venezia}
\title{Report of the\\ \textit{Advanced Programming Methods}\\ project}
\maketitle
%\doclicenseImage
%\par\medskip\noindent
%\doclicenseText
\pagenumbering{Roman}
\tableofcontents
\cleardoublepage
\pagenumbering{arabic}
\lstset{escapeinside={(*@}{@*)}}
\lstset{ keywordstyle=\bfseries,
	morekeywords={begin, true, become, end, send, to, for, sender, false, then, if, else, endif, other, right, return, while, do, end}
}
\chapter{Introduction}
This project needs to be contextualized in an algorithmic environment, the problem addressed is to transform any undirected weighted graph in a planar graph cutting edges away, in such a way that the sum of the weights of the cut edges is minimal.\\
One of the main goals was to interact with the Boost Graph Library, consequently in this paper we will focus on the following subjects: 
\begin{itemize}
\item a little of graph theory, as far as planarity condition is concerned,
\item an introduction to the Boost Graph Library
\item and finally how we addressed the problem it self.
\end{itemize}
\chapter{Graph Theory and Planar Graphs}
We revise briefly the basics of graph theory so that we can introduce in ease the planarity condition.\	\
Graphs are mathematical structures used to model pairwise relations between objects, a graph is usually said to be made up of vertices (also called nodes) which are connected by arcs (or edges). Usually we make a distinction between directed graph, where all the edges are \textit{directed} from one vertex to another and undirected graph, where all the edges are considered to be bidirectional. Among the other distinctions we can do on graphs we are particularly interested in the concept of weighted graph: a weighted graph is one in which edges have weights i.e. each edge has a numerical label.\\\\
\begin{figure}[H]
	\centering
	\begin{minipage}[c]{.45\textwidth}
		\centering\setlength{\captionmargin}{0pt}
		\includegraphics[width=1.\textwidth]{Images/img1.png}
		\caption{Directed Graph}
	\end{minipage}
	\hspace{10mm}
	\begin{minipage}[c]{.45\textwidth}
		\centering\setlength{\captionmargin}{0pt}
		\includegraphics[width=1.\textwidth]{Images/img2.png}
		\caption*{Undirected Graph}
	\end{minipage}
\end{figure}
\begin{figure}[H]
\centering
\includegraphics[width=6.5cm]{Images/img3.png}
\caption{Weighted Graph}
\end{figure}
Now that we have these basic information about graphs we can continue explaining exactly what is a \textbf{planar graph}.
\begin{definition}
In graph theory, a planar graph is a graph that can be embedded in the plane, i.e. it can be drawn on the plane in such a way that its edges intersect only at their endpoints. In other words, it can be drawn in such a way that no edges cross each other.
\end{definition}
In the following images we can see a graph that may seem not to be planar since edges $(B,D)$ and $(A,C)$ cross but in reality we can find an embedding (a different configuration that express the same information) in which those edges do not cross.
\begin{figure}[H]
\centering
\includegraphics[width=10cm]{Images/img4.png}
\caption{Planar Graph}
\end{figure}
On the contrary, in the following images we have a graph that is not planar, as a matter of fact no matter how we try to rearrange edges there will always be some edges crossing each others.
\begin{figure}[H]
\centering
\includegraphics[width=10cm]{Images/img5.png}
\caption{Not Planar Graph}
\end{figure}
Below we are going to give the definition of \textit{maximal planar graph} and the formulation of the Kuratowski's Theorem, that are the two fundamental theoretical notions from which we started reasoning in order to solve the problem presented in the introduction.\\
\paragraph*{\textsc{Kuratowski's Theorem.}} 
\begin{theorem}
Kuratowski's Theorem states that a finite graph is planar if and only if it does not contain a subgraph that is a subdivision\footnote{A subdivision of a graph is a graph formed by subdividing its edges into paths of one or more edges.} of $K_5$ (the complete graph on five vertices) or of $K_{3,3}$ (complete bipartite graph on six vertices, three of which connect to each of the other three.
\end{theorem}
\begin{figure}[H]
	\centering
	\begin{minipage}[c]{.45\textwidth}
		\centering\setlength{\captionmargin}{0pt}
		\includegraphics[width=1.\textwidth]{Images/img6.png}
		\caption{$K_5$}
	\end{minipage}
	\hspace{10mm}
	\begin{minipage}[c]{.45\textwidth}
		\centering\setlength{\captionmargin}{0pt}
		\includegraphics[width=1.\textwidth]{Images/img7.png}
		\caption{$K_{3,3}$}
	\end{minipage}
\end{figure}
In other words, a finite graph is planar if and only if it does not contain a subgraph that is homeomorphic\footnote{In graph theory, two graphs $G$and $G'$ are homeomorphic if there is a graph isomorphism from some subdivision of $G$ to some subdivision of $G'$.} to $K_5$ or $K_{3,3}$.\\
Moreover, if $G$ is a graph that contains a subgraph $H$ that is a subdivision of $K_5$ or $K_{3,3}$, then $H$ is known as a Kuratowski subgraph of G.  With this notation, Kuratowski's theorem can be expressed succinctly.
\begin{center}
A graph is planar if and only if it does not have a Kuratowski subgraph.
\end{center} 
Also notice that from an algorithmic point of view, it is possible to find a Kuratowski subgraph of a non-planar graph in linear time (as measured by the size of the input graph); this allows the correctness of a planarity testing algorithm to be verified for non-planar inputs, as it is straightforward to test whether a given subgraph is or is not a Kuratowski subgraph\footnote{Usually non-planar graph contains a large number of Kuratowski sub-graphs.}.



\paragraph*{\textsc{Maximal Planar Graph}} A simple graph is called maximal planar if it is planar but adding any edge (on the given vertex set) would destroy that property. 
\begin{definition}
If a maximal planar graph has $v$ vertices with $v>2$, then it has precisely $3v-6$ edges and $2v-4$ faces\footnote{In any planar graph, drawn with no intersections, the edges divide the planes into different regions. The regions enclosed by the planar graph are called interior faces of the graph whereas the region surrounding the planar graph is called the exterior (or infinite) face of the graph.\\
When we talk about faces of the graph we mean both the interior and the exterior ones. We usually denote the number of faces of a planar graph by $f$.}.
As a matter of fact Euler's formula states that if a finite, connected, planar graph is drawn in the plane without any edge intersections, and $v$ is the number of vertices, $e$ is the number of edges and $f$ is the number of faces then:
$$v-e+f=2.$$
Actually, using the above formula it was proven that in a finite, connected, simple and planar graph with $v\geq 3$ the number of edges is bounded by the following formula:
$e\leq 3v-6$.
\end{definition}





\chapter{Boost Graph Library}
The Boost Graph Library is a, really wide, generic\footnote{The BGL graph interface and graph components are generic, in the same sense as the Standard Template Library (STL)} library which aim is to give a generic interface that allows access to a graph's structure, but hides the details of the implementation. \\
Graphs are mathematical abstractions that are useful for solving many types of problems in computer science; for this reason it is really important to have an abstraction that makes us able to work in ease with this mathematical tools.\\
As we said above the Boost Graph Libraries offers a wide variety of methods, data structures and features however in this report we will only present the ones we used to address the problem of our interest.\\\\



\end{document}