#include "Support.hpp"
    
    //defining two variables as pair of edge_iterators
    std::pair<edge_iterator, edge_iterator> edgePair, kuratPair;

    //defining some counters that will be used later (just for "statistical" purposes)
    int testingCounter = 0, comboCounter = 0; 

    //constructor: we create a vector of tuples (each containing the edge descriptor and 
    //its corresponding weight) that will represent the graph from now on.
    //This is done in order to make a lot of operations easier.
    //Here we also decide rightaway which is the number of edges we need to cut in order
    //to obtain a planar graph (the theory beyond the formula is explained in the report of this project)
    Support::Support(gr &g) : G(g){
        EdgeWeightMap = get(boost::edge_weight_t(), G);
        for (edgePair = edges(g); edgePair.first != edgePair.second; ++edgePair.first) {
            graph.push_back(make_tuple(*edgePair.first, EdgeWeightMap[*edgePair.first]));
        }
        tocut = num_edges(G)-(3*num_vertices(G)-6);
    }

    //This function makes our graph planar. We compute all the possible combinations of "tocut" edges
    //then we choose the best one that is the one that if erased, makes our graph planar 
    //and has the minimum possible sum of the edges weight. Once we have found it we erase it once and for all.
    //Then we sum the edges weight to obtain the amount of weight we took away.
    //Notice that in case we use the Kuratowski reduction the only difference is that we start from a smaller
    //set to compute all the possible combinations, consequently we will have less combinations.
    int Support::makePlanar(bool reduction){
        my_vector res;
        my_tuple combo;
        int sum=0;
        bool* used = new bool[num_edges(G)];
        std::fill(used, used+num_edges(G), false);
        if(reduction){
            getKuratowskiReduction();
        }
        genCombos(getTocut(), 0, 0, used, &res);
        combo = getBestCombo(res);

        //We keep the following four lines of code commented because they were used for 
        //debugging, but the reader/user may be interested in seeing righaway which is 
        //the best combination.

        /*std::cout << "BEST COMBO" << std::endl;
        for(my_tuple::const_iterator j=combo.begin(); j!=combo.end(); ++j){
            std::cout << get<0>(*j) << " ";
        }
        std::cout << std::endl;*/

        eraseEdges(combo);
        for(my_tuple::const_iterator i=combo.begin(); i!=combo.end(); ++i){
            sum+=get<1>(*i);
        }
        return sum;
    }

    //This function is the one generating all the possible combinations.
    void Support::genCombos(int k, int start, int currLen, bool* used, my_vector* res){
            my_tuple combo;
            if (currLen == k){
                int j=0;
			    for (my_tuple::const_iterator i = graph.begin(); i != graph.end(); ++i) {
				    if (used[j] == true) {
                        combo.push_back(make_tuple(get<0>(*i), get<1>(*i)));
				    }
                    j++;
			    }
                res->push_back(combo);
                return;
		    }
		if (start == graph.size()) {
			return;
		}
		// For every index we have two options,
		// 1. Either we select it, means put true in used[] and make currLen+1
		used[start] = true;
		genCombos(k, start + 1, currLen + 1, used, res);
		// 2. OR we dont select it, means put false in used[] and dont increase
		// currLen
		used[start] = false;
		genCombos(k, start + 1, currLen, used, res);      
    }

    //This function is the one selecting the best combination above all.
    //To do so it sums the weight of the edges in every combination
    //if the sum is less than the previous computed sum (that initially is MAXINT)
    //then we erase those edges and we perform the planarity test, if the combination
    //passes the test we have a new temporarily best combination. Otherwise we stick 
    //with the previous found one. In both cases we add again the edges so we can 
    //continue with all the other combination as explained above.
    //Upon termination we will have selected the best combination above all.
    my_tuple Support::getBestCombo(my_vector combos){
        int sum=0, bestSum=INT_MAX;
        my_tuple bestCombo;
        for(my_vector::const_iterator i=combos.begin(); i!=combos.end(); ++i){
            comboCounter++;
            for(my_tuple::const_iterator j=i->begin(); j!=i->end(); ++j){
                sum += get<1>(*j);
            }
            if(sum<bestSum){
                eraseEdges(*i);
                if(boyer_myrvold_planarity_test(G)) {
                    testingCounter++;
                    bestCombo=*i;
                    bestSum=sum;
                }
                addEdges(*i);        
            }
            sum=0;
        }

        //We keep the following two lines of code commented because they were used for 
        //debugging, but the reader/user may be interested in seeing righaway how many 
        //planarity tests were performed and how many combinations there were.

        /*std::cout << "We have done " << testingCounter << " planarity tests";
        std::cout << " and there were " << comboCounter << " combinations" << std::endl;*/
        return bestCombo;
    }


    //This function reduces the set (our graph) from which we compute combinations to a smaller one.
    //The theory on which this function relies on is explained in the report of this project.
    //Unfortunately, this function is not as efficient as we wish because Boost graph library
    //function "boyer_myrvold_planarity_test" wants a graph with no weights in order for us
    //to take advantage of the collateral effect that make us obtain the Kuratowski subgraph.
    //However for the purposes of this project it was fundamental for us to have a weighted graph 
    //consequently we had to create another graph (without weights) with the same edge descriptors
    //of our graph and than once we obtained the kuratowski subgraph we delete from our original 
    //graph the edged that are not in the Kuratowski subgraph.
    void Support::getKuratowskiReduction(){
        kuratowski_edges_t kuratowski_edges;
        kuratowskiGraph neo(num_vertices(G));
        graph_traits<kuratowskiGraph>::edges_size_type edge_count = 0;
        graph_traits<kuratowskiGraph>::edge_iterator ei, ei_end;
        for(edgePair = edges(G); edgePair.first != edgePair.second; ++edgePair.first){ 
            add_edge(source(*edgePair.first, G), target(*edgePair.first, G), neo);
        }
        property_map<kuratowskiGraph, edge_index_t>::type e_index = get(edge_index, neo);
        for(tie(ei, ei_end) = edges(neo); ei != ei_end; ++ei){
            put(e_index, *ei, edge_count++);
            
        }
        boyer_myrvold_planarity_test(boyer_myrvold_params::graph = neo,boyer_myrvold_params::kuratowski_subgraph = std::back_inserter(kuratowski_edges));
        
        //We keep the following seven lines of code commented because they were used for 
        //debugging, but the reader/user may be interested in seeing righaway which 
        //are the edges in the Kuratowski subgraph.

        /*std::cout << "Edges in the Kuratowski subgraph: ";
        kuratowski_edges_t::iterator ki, ki_end;
        ki_end = kuratowski_edges.end();
        for(ki = kuratowski_edges.begin(); ki != ki_end; ++ki){
          std::cout << *ki << " ";
        }
        std::cout << std::endl;*/


        bool found = false;
        for(my_tuple::const_iterator i = graph.begin(); i!=graph.end(); ++i){  
            graph_traits<gr>::edge_descriptor x = get<0>(*i); 
            for(kuratowski_edges_t::const_iterator j = kuratowski_edges.begin(); j!=kuratowski_edges.end() && !found; j++){
                graph_traits<gr>::edge_descriptor k = *j;
                if((x.m_source == k.m_source) && (x.m_target == k.m_target)){
                    found=true;
                }
            }
            if(!found){
                graph.erase(i);
                i--;
            }else{
                found=false;
            }            
           
        }
        
    }

   
    //This function erases from a graph all the edges contained in a particular combination
    //passed as input.
    void Support::eraseEdges(my_tuple combo){
        for(my_tuple::const_iterator i = combo.begin(); i != combo.end(); ++i){
            remove_edge(source(get<0>(*i), G), target(get<0>(*i), G), G);
        }
    }

    //This function adds in a graph all the edges contained in a particular combination
    //passed as input.
    void Support::addEdges(my_tuple combo){
        VertexDescriptor u,v;
        gr::edge_descriptor e;
        for(my_tuple::const_iterator i = combo.begin(); i != combo.end(); ++i){
            e=get<0>(*i);
            u = source(e, G);
            v = target(e, G);
           add_edge(u, v, get<1>(*i), G);   
        }
    }

    //This function prints the graph represented as a vector of tuple
    void Support::printGraph(){
    for (my_tuple::const_iterator i = graph.begin(); i != graph.end(); ++i) {
            std::cout << "Edge: " << get<0>(*i) << " Weight: " << get<1>(*i) << std::endl;
        }
    }

    //This function prints the Boost graph
    void Support::printBoostGraph(){
        for (edgePair = edges(G); edgePair.first != edgePair.second; ++edgePair.first) {
            std::cout << *edgePair.first << " " << EdgeWeightMap[*edgePair.first] << std::endl;
        } 
    }

    //This function performs the planarity test
    bool Support::isPlanar(){
        return boyer_myrvold_planarity_test(G);
    }

    //This function returns the number of edges we need to cut
    int Support::getTocut(){
        return tocut;
    }
